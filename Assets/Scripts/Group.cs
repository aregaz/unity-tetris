using System;
using Assets.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Group : MonoBehaviour
{
    // Time since last gravity tick
    private int _counter = 0;

    public float HorizontalSpeed = 2f;
    public float VerticalSpeed = 1f;

    public AudioSource MovementSoundLeft { get; set; }
    public AudioSource MovementSoundRight { get; set; }
    public AudioSource DownSound { get; set; }
    public AudioSource ClearLineSound { get; set; }
    public AudioSource RotationSound { get; set; }

    private float _lastFallTime = 0f;

    private float _lastLeftMoveTime = 0f;
    private float _lastRightMoveTime = 0f;
    private float _lastRotationTime = 0f;
    private float _xSum = 0f;

    public static event CustomEvent.OnXMovement XMove;

    // Start is called before the first frame update
    void Start()
    {
        // Default position not valid? Then it's game over
        if (!IsValidGridPosition())
        {
            Debug.Log("GAME OVER");
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // _counter++;
        // Debug.Log($"Group.Update() says {_counter}");
        float xInput = Input.GetAxis("Horizontal");
        float yInput = Input.GetAxis("Vertical");

        if (xInput == 0)
        {
            _lastLeftMoveTime = 0;
            _xSum = 0;
        }

        // Move Left
        if (xInput < 0)
        {
            if (Time.time - _lastLeftMoveTime > Spawner.GetXInterval())
            {
                // Modify position
                transform.position += new Vector3(-1, 0, 0);

                // See if valid
                if (IsValidGridPosition())
                {
                    // It's valid. Update grid.
                    UpdateGrid();
                    XMove?.Invoke(
                        this.name,
                        new XMovementArgs
                        {
                            Direction = XMoveDirection.Left,
                        });
                }
                else
                {
                    // It's not valid. revert.
                    transform.position += new Vector3(1, 0, 0);
                }

                MovementSoundLeft?.Play();
                _lastLeftMoveTime = Time.time;
            }
        }

        // Move Right
        else if (xInput > 0)
        {
            if (Time.time - _lastRightMoveTime > Spawner.GetXInterval())
            {
                // Modify position
                transform.position += new Vector3(1, 0, 0);

                // See if valid
                if (IsValidGridPosition())
                {
                    // It's valid. Update grid.
                    UpdateGrid();
                    XMove?.Invoke(
                        this.name,
                        new XMovementArgs
                        {
                            Direction = XMoveDirection.Right,
                        });
                }
                else
                {
                    // It's not valid. revert.
                    transform.position += new Vector3(-1, 0, 0);
                }

                MovementSoundRight?.Play();
                _lastRightMoveTime = Time.time;
            }
        }

        // no horizontal move
        else
        {
            _lastLeftMoveTime = 0f;
            _lastRightMoveTime = 0f;
        }

        // Rotate
        if (yInput > 0 && Time.time - _lastRotationTime > 0.3f)
        {
            transform.Rotate(0, 0, -90);

            // See if valid
            if (IsValidGridPosition())
            {
                // It's valid. Update grid.
                UpdateGrid();
                RotationSound.Play();
            }
            else
                // It's not valid. revert.
                transform.Rotate(0, 0, 90);

            _lastRotationTime = Time.time;
        }

        // Move Downwards and Fall
        if (yInput < 0 &&
            Time.time - _lastFallTime >= Spawner.GetYInterval())
        {
            MoveDown(playSound: true);
        }

        if (Time.time - _lastFallTime >= 1)
        {
            MoveDown(playSound: false);
        }
    }

    private void MoveDown(bool playSound)
    {
        // Modify position
        transform.position += new Vector3(0, -1, 0);

        // See if valid
        if (IsValidGridPosition())
        {
            // It's valid. Update grid.
            UpdateGrid();
            if (playSound)
            {
                DownSound.Stop();
                DownSound.Play();
            }
        }
        else
        {
            // It's not valid. revert.
            transform.position += new Vector3(0, 1, 0);

            // Clear filled horizontal lines
            bool isRowsDeleted = Playfield.DeleteAllFullRows();
            if (isRowsDeleted)
            {
                ClearLineSound.Play();
            }

            UnsubscribeFromEvents();

            // Spawn next Group
            FindObjectOfType<Spawner>().SpawnNext();

            // Disable script
            enabled = false;
        }

        _lastFallTime = Time.time;
    }

    private bool IsValidGridPosition()
    {
        foreach (Transform child in transform)
        {
            Vector2 v = Playfield.RoundVec2(child.position);

            // Not inside Border?
            if (!Playfield.IsInsideBorder(v))
                return false;

            // Block in grid cell (and not part of same group)?
            if (Playfield.grid[(int)v.x, (int)v.y] != null &&
                Playfield.grid[(int)v.x, (int)v.y].parent != transform)
                return false;
        }

        return true;
    }

    private void UpdateGrid()
    {
        // Remove old children from grid
        for (int y = 0; y < Playfield.h; ++y)
        for (int x = 0; x < Playfield.w; ++x)
            if (Playfield.grid[x, y] != null)
                if (Playfield.grid[x, y].parent == transform)
                    Playfield.grid[x, y] = null;

        // Add new children to grid
        foreach (Transform child in transform)
        {
            Vector2 v = Playfield.RoundVec2(child.position);
            Playfield.grid[(int)v.x, (int)v.y] = child;
        }
    }

    private float GetXInterval()
    {
        if (Math.Abs(_xSum) > 1)
        {
            return 0.6f;
        }

        return 1f;
    }

    private void UnsubscribeFromEvents()
    {
        Delegate[] xMoveSubscribers = XMove?.GetInvocationList() ?? Array.Empty<Delegate>();
        foreach (Delegate subscriber in xMoveSubscribers)
        {
            XMove -= subscriber as CustomEvent.OnXMovement;
        }

        Debug.Log($"Unsubscribed from XMove - {name}");
    }
}
