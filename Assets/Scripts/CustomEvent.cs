﻿namespace Assets.Scripts
{
    public class XMovementArgs
    {
        public XMoveDirection Direction { get; set; }
    }

    public class CustomEvent
    {
        public delegate void OnXMovement(string sender, XMovementArgs args);
    }
}
