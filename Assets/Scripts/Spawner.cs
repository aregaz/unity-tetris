using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    // Groups
    public GameObject[] groups;

    public AudioSource MovementSoundLeft;
    public AudioSource MovementSoundRight;
    public AudioSource DownSound;
    public AudioSource ClearLineSound;
    public AudioSource RotationSound;

    public TextMeshProUGUI XInputText;
    public TextMeshProUGUI YInputText;

    public TextMeshProUGUI XSumText;
    public TextMeshProUGUI YSumText;

    public TextMeshProUGUI XInterval;
    public TextMeshProUGUI YInterval;

    private const float XSumThreshold = 100;
    private const float YSumThreshold = 100;

    private float _previousX = 0;
    private float _previousY = 0;

    private static float _xSum = 0;
    private static float _ySum = 0;

    // Start is called before the first frame update

    void Start()
    {
        SpawnNext();
    }

    // Update is called once per frame
    void Update()
    {
        float xInput = Input.GetAxis("Horizontal");
        float yInput = Input.GetAxis("Vertical");

        float intervalX = GetXInterval();
        float intervalY = GetYInterval();

        if (xInput == 0 || Math.Abs(xInput) < Math.Abs(_previousX))
        {
            _xSum = 0;
        }
        else if ((xInput < 0 && _previousX < 0 && Math.Abs(xInput) > Math.Abs(_previousX)) ||
                 (xInput > 0 && _previousX > 0 && Math.Abs(xInput) > Math.Abs(_previousX)))
        {
            _xSum += xInput;
        }

        if (yInput == 0 || Math.Abs(yInput) < Math.Abs(_previousY))
        {
            _ySum = 0;
        }
        else if ((yInput < 0 && _previousY < 0 && Math.Abs(yInput) > Math.Abs(_previousY)) ||
                 (yInput > 0 && _previousY > 0 && Math.Abs(yInput) > Math.Abs(_previousY)))
        {
            _ySum += yInput;
        }

        XInputText.text = $"X input: {xInput * 100 * Time.deltaTime}";
        YInputText.text = $"Y input: {yInput * 100 * Time.deltaTime}";

        XSumText.text = $"X sum: {_xSum} of {XSumThreshold}";
        YSumText.text = $"Y sum: {_ySum} of {YSumThreshold}";

        XInterval.text = $"X interval: {intervalX} sec";
        YInterval.text = $"Y interval: {intervalY} sec";

        _previousX = xInput;
        _previousY = yInput;
    }

    public static float GetXInterval()
    {
        return Math.Abs(_xSum) > 100f ? 0.05f : 1f;
    }

    public static float GetYInterval()
    {
        return Math.Abs(_ySum) > 100f ? 0.1f : 1f;
    }

    public void SpawnNext()
    {
        // Random Index
        int i = Random.Range(0, groups.Length);

        // Spawn Group at current Position
        GameObject spawnedGameObject = Instantiate(
            groups[i],
            transform.position,
            Quaternion.identity);
        Group group = spawnedGameObject.GetComponent<Group>();
        group.MovementSoundLeft = MovementSoundLeft;
        group.MovementSoundRight = MovementSoundRight;
        group.DownSound = DownSound;
        group.ClearLineSound = ClearLineSound;
        group.RotationSound = RotationSound;

        Debug.Log($"Spawned new {spawnedGameObject.name}");
    }

    // private void OnXMove(XMoveDirection direction)
    // {
    //     if (direction == XMoveDirection.Left)
    //     {
    //         MovementSoundLeft.Play();
    //     }
    //     else
    //     {
    //         MovementSoundRight.Play();
    //     }
    // }
}
